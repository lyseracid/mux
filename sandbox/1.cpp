#include "h.hpp"

int main(int, const char** argv) {
    const mux::Args args(argv);
    mux::print("Run", args.binary);
    for (const auto& arg: args.args)
        mux::Traceble<std::string>(arg).print();
}

