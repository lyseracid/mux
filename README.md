# mux: MU eXecutables

---

## Directory

- *config* -- configuration files stash
- *sandbox* -- playground for fast tests
- *scripts* -- some usefull scripts (bash, python, etc)
- *NOTE.md* -- just a working notebook

## Tools Usage

- `$ config/manage.sh` -- check differences with stored configs
- `$ config/manage.sh pull` -- update all local configs
- `$ scripts/install.sh` -- installs script symlinks into /usr/local/bin

## Fast start

- `$ mux/fast-start.sh` -- installs tools and some ubuntu packages
