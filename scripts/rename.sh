#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Replaces substring for all files in current directory.
Usage:
    [MV=mv] [R=1] $0 SUBSTRING REPLACENEMT
END
exit 0; fi

set -e
[ "$X" ] && set -x

MV=${MV:-mv}

FILES="ls"
[ "$R" ] && FILES="find -type f"

ROLLBACK=/tmp/mux-rename.rollback
echo '#!/bin/bash' > $ROLLBACK
chmod +x $ROLLBACK

$FILES | while read -d $'\n' NAME; do
    RENAME=$(python -c "print('''$NAME'''.replace('''$1''', '''$2'''))")
    [[ "$NAME" == "$RENAME" ]] && continue

    echo Rename: $NAME
    echo --- to: $RENAME

    $MV "$NAME" "$RENAME"
    echo $MV \"$RENAME\" \"$NAME\" >> $ROLLBACK
done

echo echo Rolled back >> $ROLLBACK
echo Done, rollback: $ROLLBACK
