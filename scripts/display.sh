#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Sets up a halfed resolution on a laptop display.
Usage: [OVERRIDE=VALUE] $0 [-y] [SCALE_RES] [SCALE_OPT]
Options:
    -y  Do not ask for confirmation, default: ask
Arguments:
    SCALE_RES   Scaledown resolution ratio, default: 2 (times)
    SCALE_OPT   Scale xrandr ratio, default: 1
Overrides:
    D   Display name to setup, example eDP-1, default: fetch Screen 0 name
    M   Max Resolution, example: 2880x1800, default: fetch Screen 0 resolution

END
exit 0; fi

source "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))/resources/tools.sh"

set -e
[ "$X" ] && set -x

if [[ "x$1" == "x-y" ]]; then
    NO_CONFIRMATION=1
    shift
fi

DISP=${D:-$(xrandr | sed -n '2p' | awk '{print$1}')}
MAX_RES=${R:-$(xrandr | sed -n '3p' | awk '{print$1}')}
SCALE_RES=${1:-2}
SCALE_OPT=$(python -c "print(1 / ${2:-1})")
SCALE_OPT=${SCALE_OPT}x${SCALE_OPT}
echo Display: $DISP, Max Resolution: $MAX_RES
echo Scale Resolution: $SCALE_RES, Scale Option: $SCALE_OPT


MAX_W=$(echo $MAX_RES | cut -dx -f1); TGT_W=$((MAX_W/$SCALE_RES))
MAX_H=$(echo $MAX_RES | cut -dx -f2); TGT_H=$((MAX_H/$SCALE_RES))
TGT_MODE=$(cvt $TGT_W $TGT_H | sed -n '2p' | awk '{for (i=2; i<=NF; i++) print $i}')
TGT_RES=$(echo $TGT_MODE | awk '{print$1}')
echo New Mode: $TGT_RES
echo $TGT_MODE

if [[ ! $NO_CONFIRMATION ]]; then
    mux_confirm "Apply Changes?" || exit
fi

mux_trace_run xrandr --newmode $TGT_MODE || true #< Fails if the mode is already added.
mux_trace_run xrandr --addmode $DISP $TGT_RES
mux_trace_run xrandr --output $DISP --mode $TGT_RES --scale $SCALE_OPT

