#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Diffs charaters by blocks in strings
Usage: [C=2] $0 [SRING1] [STRING2]
END
exit 0; fi

set -e
[ "$X" ] && set -x

COUNT=${C:-1}

BASE=/tmp/mux-cdiff-$(date +%s)
LEFT=$BASE-left
RIGHT=$BASE-right

DOTS=$(printf "%0.s." $(seq 1 $COUNT))
PATTERN="s/\($DOTS\)/\1\n/g"

sed -e "$PATTERN" <<< "$1" > $LEFT
sed -e "$PATTERN" <<< "$2" > $RIGHT

diff $LEFT $RIGHT
rm $LEFT $RIGHT
