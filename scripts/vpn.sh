#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Starts openvpn in background and restarts if required
Usage:
    [OPTION=VALUE] $0 [CONFIG_NUMBER]
    Options:
        D   Config directory, default: ~/.openvpn
        C   Config name patern, default: *
        CH  Control host (for ping), default: google.com
        CA  Control attempts (for ping), default: 2
END
exit 0; fi

DIR=${D:-"$HOME/.openvpn"}
if [ $(whoami) != root ]; then
    sudo D=$DIR C=$C CH=$CH $0 "$@"
    exit $?
fi

set -e
[ "$X" ] && set -x
source "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))/resources/tools.sh"

CONFIGS=*$C*.ovpn
CONTROL_HOST=${CH:-google.com}
CONTROL_ATTEMPTS=${CA:-2}

cd $DIR
IFS=', ' read -r -a CONFIGS <<< $(echo $CONFIGS)
[ "${#CONFIGS[@]}" == 0 ] && mux_fail No configs in $DIR

if [ "${#CONFIGS[@]}" == 1 ]; then
    CONFIG=${CONFIGS[0]}
elif [ ! -z $1 ]; then
    CONFIG=${CONFIGS[$1]}
else
    for (( i = 0; i < ${#CONFIGS[@]}; i++ )); do echo "$i: ${CONFIGS[$i]}"; done
    read -p "Select config: " i
    CONFIG=${CONFIGS[i]}
fi

[ ! $CONFIG ] && mux_fail Config is not selected
[ ! -f $CONFIG ] && mux_fail Config does not exist $PWD/$CONFIG

mux_title "Running $CONFIG controlled by $CONTROL_HOST ($CONTROL_ATTEMPTS)"
grep 'pull-filter ignore redirect-gateway' $CONFIG >/dev/null \
    && mux_warn Traffic routing is disabled by config
grep 'redirect-gateway def1 bypass-dhcp' $CONFIG >/dev/null \
    && mux_warn Traffic routing is enforced by config

while true; do
    sleep 1
    mux_trace_run openvpn $CONFIG &
    sleep 1
    function onCtrlC {
        mux_trace_run pkill -KILL openvpn
        sleep 1 #< TODO: Wait for process exit the other way.
        exit 0
    }
    trap onCtrlC SIGINT
    while pgrep openvpn >/dev/null; do
        if ! ping -c $CONTROL_ATTEMPTS $CONTROL_HOST >/dev/null 2>&1; then
            mux_trace_run pkill -KILL openvpn
            break
        fi
    done
done


