#!/usr/bin/env python

import json
import os
from collections import OrderedDict
from enum import Enum


class ListEditor:
    def __init__(self, data):
        self.data = data

    def add_unique(self, target_field, target_name, **extra_data):
        if [c for c in self.data if c[target_field] == target_name]:
            raise ValueError(f"Target {target_name} is already there")
        new_data = OrderedDict(**{target_field: target_name})
        new_data.update(extra_data)
        self.data.append(new_data)
        return new_data

class ConfigMode(Enum):
    remove = 'remove'
    create = 'create'
    edit = 'edit'


class ConfigEditor:
    def __init__(self, path, list_name = None, mode = ConfigMode.edit):
        self.path = path
        self.list_name = list_name
        self.mode = mode

    def __enter__(self):
        if self.mode is ConfigMode.remove:
            print(f"Remove old {self.path} {self.list_name}")
            os.remove(self.path)
        try:
            with open(self.path, 'r') as fd:
                print(f"Edit {self.path} {self.list_name}")
                text = ''.join(line for line in fd if not line.strip().startswith('//'))
                self.data = json.loads(text, object_pairs_hook=OrderedDict)
        except FileNotFoundError:
            if self.mode is ConfigMode.edit:
                raise
            print(f"Creating new {self.path} {self.list_name}")
            self.data = {}
            if self.list_name:
                self.data.setdefault(self.list_name, [])
        return ListEditor(self.data[self.list_name]) if self.list_name else self.data

    def __exit__(self, *args):
        with open(self.path, 'w') as fd:
            json.dump(self.data, fd, indent=4)
        print(f"Saved {self.path} {self.list_name}")


def add_debug(target, args = [], type = 'lldb', config_mode = ConfigMode.edit):
    with ConfigEditor('.vscode/launch.json', 'configurations', config_mode) as configs:
        config = configs.add_unique(
            'name', target,
            type = type,
            request = 'launch',
            cwd = '${fileDirname}',
            program = '${workspaceFolder}-build/bin/' + target,
            args = args
        )


def add_cmake(target, args = [], config_mode = ConfigMode.edit):
    with ConfigEditor('.vscode/tasks.json', 'tasks', config_mode) as configs:
        config = configs.add_unique(
            'label', target,
            command = 'mux-cmake',
            args = [target] + args,
            problemMatcher = ['$gcc'],
        )


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='VS Code Target Editor')
    parser.add_argument('target')
    parser.add_argument('args', nargs='*')
    parser.add_argument('-t', '--task', action='store_true', default=False,
        help='Add mux-cmake task to build and run')
    parser.add_argument('-d', '--debug', action='store_true', default=False,
        help='Add debug configuration for lldb')
    parser.add_argument('-c', '--create-config', action='store_true', default=False,
        help='Create configuration file if it does not exist')
    parser.add_argument('-r', '--remove-config', action='store_true', default=False,
        help='Remove existing configuration file on start')

    args = parser.parse_args()
    config_mode = ConfigMode.remove if args.remove_config else \
        ConfigMode.create if args.create_config else \
        ConfigMode.edit
    if args.debug: add_debug(args.target, args.args, config_mode)
    if args.task: add_cmake(args.target, args.args, config_mode)
