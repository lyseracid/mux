#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
CMake build helper script.
Usage:
    [OPTION=VALUE ...] $0 [target] [run-arguments]
Options:
    R   build release instead of debug (affects build directory)
    C   clean generate & build if set
    B   build directory, default: $PWD-build
    G   cmake generate flags to set
    FG  force cmake generate (enabled on G)
    K   keep current terminal output
    TC  number of threads to build on, eg -j, overrides TS
    TS  threads to save, default ${MUX_THREAD_SAVE:-0}
    L   log directoy to store {g|b|r}.log files, default: none
    N   do not use ninja build
Examples:
    B=./build G="-Dparameter=value" $0
    $0 some_module_ut --gtest_filter="SomeTest*"
GTest Target Aliases:
    $0 TARGET/CASE[/TEST][/b]
        $0 TARGET --gtest_filter=CASE[*TEST] [--break_on_failure]
    $0 some_module_ut/SomeTestCase/SomeTestName
        $0 some_module_ut --gtest_filter=SomeTestCase*SomeTestName
    $0 some_module_ut/Some*/b
        $0 some_module_ut --gtest_filter=Some* --break_on_failure

END
exit 0; fi

source "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))/resources/tools.sh"

set -e
[ ! "$K" ] && clear
[ "$X" ] && set -x

function log() {
    [[ "$L" ]] && echo "$L/$1.log"
}

# ===============================================
# Preprocess GTest extended target argument.

if [ "$1" ]; then
    IFS=/ read -ra TARGET_LINE <<< "$1"
    shift

    TARGET="${TARGET_LINE[0]}"
    unset TARGET_LINE[0]

    RUN_ARGS="$@"
    if [[ ${#TARGET_LINE[@]} != 0 ]] && [[ "${TARGET_LINE[-1]}" == b ]]; then
        RUN_ARGS="--gtest_break_on_failure $RUN_ARGS"
        unset TARGET_LINE[-1]
    fi

    if [[ ${#TARGET_LINE[@]} != 0 ]]; then
        FILTER=$(echo ${TARGET_LINE[@]} | tr ' ' '*')
        RUN_ARGS="--gtest_filter=$FILTER $RUN_ARGS"
    fi
fi

# ===============================================
# Generate CMake project.

GENERATE_FLAGS=($G $MUX_CMAKE_G)
IS_RELEASE=$R

SOURCE_DIR="$PWD"
mux_is_windows && SOURCE_DIR=../$(basename "$PWD")

BUILD_DIR_SUFFIX=""
[ "$IS_RELEASE" ] && BUILD_DIR_SUFFIX+='-release'

BUILD_DIR=${B:-"$SOURCE_DIR-build$BUILD_DIR_SUFFIX"}
[ "$C" ] && mux_trace_run rm -r "$BUILD_DIR"

FORCE_GENERATE="$FG$F"
if [ "$FORCE_GENERATE" ]; then
    [ "FORCE_GENERATE" ] && echo Force Generate!
    rm "$BUILD_DIR/CMakeCache.txt" || true
fi

if [ "$GENERATE_FLAGS" ] || [ ! -f "$BUILD_DIR/CMakeCache.txt" ]; then
    mkdir -p "$BUILD_DIR"
    [ "$IS_RELEASE" ] && GENERATE_FLAGS+=(-DCMAKE_BUILD_TYPE=Release)
    if which ninja 2>/dev/null; then
        GENERATE_FLAGS+=(-GNinja)
    elif mux_is_windows; then
        GENERATE_FLAGS+=(-Ax64 -Thost=x64)
    fi
    if ! mux_is_windows; then
        GENERATE_FLAGS+=(-DCMAKE_CXX_FLAGS=' -fdiagnostics-color=always')
    fi
    GENERATE_FLAGS+=(-DcolorOutput=1)
	cd "$BUILD_DIR"
	LOG=$(log g) mux_trace_run cmake "$SOURCE_DIR" "${GENERATE_FLAGS[@]}"
	cd -
fi

# ===============================================
# ===== Build CMake target.

BUILD_FLAGS="--build $BUILD_DIR"
[ "$TARGET" ] && BUILD_FLAGS+=" --target $TARGET"

if mux_is_linux; then
    THREAD_COUNT=${TC:-$(( $(nproc) - ${TS:-${MUX_THREAD_SAVE:-0}} ))}
    BUILD_FLAGS+=" -- -j $THREAD_COUNT"
fi
if mux_is_windows && [ "$IS_RELEASE" ]; then
    BUILD_FLAGS+=" --config Release"
fi

LOG=$(log b) mux_trace_run cmake $BUILD_FLAGS

# ===============================================
# ===== Run built CMake target.

echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
if [[ "$RUN_ARGS" ]]; then
    mux_is_windows && TARGET+=.exe
    BINARY_PATH=$(find "$BUILD_DIR" -name "$TARGET" -type f | head -1)
    [[ "x" == "x$BINARY_PATH" ]] && mux_fail No executable $BUILD_DIR .. $TARGET
    LOG=$(log r) mux_trace_run "$BINARY_PATH" $RUN_ARGS
fi

