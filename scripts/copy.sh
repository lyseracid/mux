#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Copy/move from source to destination with progress bar.
Usage: $0 [MODE] SOURCE DESTINATION
Modes:
      Default: just make a copy.
  -m  Move: remove source after complete copy.
  -l  Symlink: make symlink after complete move.
END
exit 0; fi

source "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))/resources/tools.sh"

set -e
[ "$X" ] && set -x

MOVE=
LINK=
if [ "$1" == "-m" ] || [ "$1" == "-l" ] ; then
    MOVE=1
    [ "$1" == "-l" ] && LINK=1
    shift
fi

SOURCE="${1%/}"
DESTINATION="${2%/}"
[ -e "$DESTINATION" ] && DESTINATION+="/"$(basename "$SOURCE")

RSYNC_OPTIONS="-ah --progress"
mux_is_linux && RSYNC_OPTIONS+=" --info=progress2"

if ! mux_trace_run rsync $RSYNC_OPTIONS "$SOURCE" "$DESTINATION"; then
    rm -rf "$DESTINATION"
    exit 1
fi

[ "$MOVE" ] && mux_trace_run rm -rf "$SOURCE"
[ "$LINK" ] && mux_trace_run ln -s "$DESTINATION" "$SOURCE"
