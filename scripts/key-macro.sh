#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Hotkey macro executor.
Usage:
    $0 [command [args...]]
Commands:
    press [key] - press some keyboard key
    discord_rm - remove discord message
    cycle [command [args...]] - execute command in cycle untill next start
END
exit 0; fi

set -e
[ "$X" ] && set -x

function log() {
    echo $(date '+%Y-%m-%d %H:%M:%S') "$@" | tee -a /tmp/key-macro.log
}

function press() {
    xdotool key "$@"
    sleep 0.1
}

function discord_rm() {
    press --clearmodifiers Up
    press ctrl+a
    press BackSpace
    press Return
    press Return
    sleep 0.3
    log Removed

    press Return
    press BackSpace
    sleep 0.3
}

function cycle() {
    local RUN_FILE=/tmp/key-macro.run
    if [ -f $RUN_FILE ]; then
        log "Already running, stop it!"
        rm $RUN_FILE
        exit
    fi

    touch $RUN_FILE
    log "Start cyclic run"
    while [ -f $RUN_FILE ]; do
        "$@"
        log "Working..."
    done
    log Done
}

"$@"

