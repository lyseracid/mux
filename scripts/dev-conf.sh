#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Configurates laptop devices
Usage: $0 [<PARAM>=<VALUE> ...]
Params:
    T - touchpad (enabled/disabled), default: 1
    S - audio sinks numbers, default: 0 1
    V - audio volume (%), default: 150
END
exit 0; fi

source "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))/resources/tools.sh"

set -e
[ "$X" ] && set -x

for PARAM in $@; do
    eval $PARAM
done

TOUCHPAD=${T:-${TP:-${TOUCHPAD:-${MUX_TOUCHPAD:-1}}}}
SINKS=${S:-${SINKS:-0 1}}
VOLUME=${V:-${VOL:-${VOLUME:-${MUX_VOLUME:-150}}}}

mux_title "Touchpad: set enabled: $TOUCHPAD"
mux_trace_run xinput set-prop 12 "Device Enabled" $TOUCHPAD

mux_title "Audio: set volume: $VOLUME%"
for SINK in $SINKS; do
    mux_trace_run pactl -- set-sink-volume $SINK $VOLUME% || true
done

