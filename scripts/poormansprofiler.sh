#!/bin/bash
if [[ "$1" == --help ]] || [[ "$1" == -h ]] || [[ ! "$1" ]]; then cat <<END
Poor man's profiler: https://poormansprofiler.org
Usage: [C=COUNT] [S=SLEEP] $0 PID|PGREP
END
exit 0; fi

set -e
[ "$X" ] && set -x

COUNT=${C:-10}
SLEEP=${S:-1}

if [[ "$1" =~ ^[0-9]+$ ]]; then
    PID=$1
else
    PID=$(pgrep $1 | tail -1)
fi

for N in $(seq 1 $COUNT)
do
    echo Analyzing process $PID ... $N/$COUNT >&2
    gdb -ex "set pagination 0" -ex "thread apply all bt" -batch -p $PID 2>/dev/null || exit $?
    sleep $SLEEP
done \
| awk '
    BEGIN { s = ""; }
    /^Thread/ { print s; s = ""; }
    /^\#/ { s = s "|"; for (i = 4; i <= NF && substr($i, 1, 2) != "("; i++) s = s " " $i }
    END { print s }' \
| sort | uniq -c | sort -r -n -k 1,1 | tr "|" "\n"

