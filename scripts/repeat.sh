#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Repeats command every second until Ctrl+C
[OPTION=value] mux-repeat COMMAND [ARGS...]
Options:
    L - limit count, default: inf
    D - delay, default: 1s
    I - set to ignor errors
    V - set for verbose mode
END
exit 0; fi

source "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))/resources/tools.sh"

set -e
[ "$X" ] && set -x

LIMIT=${L:-inf}
DELAY=${D:-1s}

function onCtrlC {
    echo Interupted!
    exit 0
}
trap onCtrlC SIGINT

ITER=0
while [ "$ITER" != "$LIMIT" ]; do
    [ $V ] && COLOR=35 mux_title Run at $(date)
    if mux_trace_run "$@"; then
        COLOR=32 mux_title ok
    else
        COLOR=31 mux_title failed
        [ ! $I ] && exit
    fi

    [ $V ] && COLOR=35 mux_title Wait "for" $DELAY
    sleep $DELAY
    (( ITER++ )) || true
done
