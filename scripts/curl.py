#!/usr/bin/env python

import logging
import subprocess

_logger = logging.getLogger(__name__)


def format_input(text):
    try:
        import yaml, json
        try:
            data = yaml.load(text, Loader=yaml.FullLoader)
            return json.dumps(data)
        except yaml.YAMLError:
            return text
    except:
        return text


def format_output(text):
    try:
        import yaml
        try:
            data = yaml.load(text, Loader=yaml.FullLoader)
        except yaml.YAMLError:
            return text
        return yaml.safe_dump(data, default_flow_style=False, width=1000) if data else ''

    except ImportError:
        import json
        try:
            data = json.loads(data)
        except json.JSONDecodeError:
            return text
        return json.dumps(data, separators=(',', ':')) if data else ''


def command(args, format_data=None, host='', content_type='', auth_token=''):
    header_indexes = [i for i, e in enumerate(args) if e == '-H' or e == '--header']
    headers = [args[i + 1] for i in header_indexes if i + 1 < len(args)]

    if host and args and not args[0].startswith('http') and args[0].startswith('/'):
        args = [host + args[0]] + args[1:]

    try:
        data_index = (args.index('-d') + 1) or (args.index('--data') + 1)
        if format_data:
            args = args[:data_index] + [format_data(args[data_index])] + args[data_index+1:]
        if content_type and not [h for h in headers if h.lower().startswith('content-type')]:
            args += ['-H', 'Content-Type: {}'.format(content_type)]
    except ValueError:
        pass

    if auth_token and not [h for h in headers if h.lower().startswith('authorization')]:
        args += ['-H', 'Authorization: Bearer {}'.format(auth_token)]

    return ['curl', '-ks'] + args


def run(args, debug_out=None, format_stdout=None, *largs, **kwargs):
    c = command(args, *largs, **kwargs)
    if debug_out:
        debug_out.write('run: {!r}\n'.format(c))

    r = subprocess.run(c, capture_output=True)
    if '-i' in args or '--include' in args:
        headers, r.stdout = r.stdout.split('\r\n\r\n', 2)
        r.stderr = headers + '\r\n\r\n' + r.stderr
    if format_stdout:
        r.stdout = format_stdout(r.stdout)
        if isinstance(r.stdout, str):
            r.stdout = r.stdout.encode('utf-8')

    if debug_out:
        debug_out.write('result: {!r}\n'.format(r.returncode))
    return r


if __name__ == '__main__':
    import sys, os

    args = sys.argv[1:]
    need_help = not args or '-h' in args or '--help' in args or '--manual' in args
    if need_help:
        print('CURL wrapper with env-based argument autocomplete', file=sys.stderr)
        print('    H, MUX_CURL_HOST - host and port, default localhost', file=sys.stderr)
        print('    F, MUX_CURL_FORMAT - input format, default application/json', file=sys.stderr)
        print('    T, MUX_CURL_TOKEN - bearer token for authorization', file=sys.stderr)
        print('    R, MUX_CURL_RAW_OUT - if set output is not formatted', file=sys.stderr)

    raw_out = os.getenv('R') or os.getenv('MUX_CURL_RAW_OUT')
    result = run(
        args,
        debug_out=sys.stderr,
        format_data=format_input if not need_help else None,
        format_stdout=format_output if not (need_help or raw_out) else None,
        host=os.getenv('H') or os.getenv('MUX_CURL_HOST') or 'localhost',
        content_type=os.getenv('F') or os.getenv('MUX_CURL_FORMAT') or 'application/json',
        auth_token=os.getenv('T') or os.getenv('MUX_CURL_TOKEN'),
    )

    sys.stderr.buffer.write(result.stderr)
    sys.stdout.buffer.write(result.stdout)
    exit(result.returncode)

