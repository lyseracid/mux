#!/bin/bash

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Git rebase by cherry-peak to avoid unnecessury rebuild
Usage: $0 [CURRENT_BRANCH] [BASE_BRANCH] [MISSING_COMMIT_HASHES]
END
exit 0; fi

set -e
[ "$X" ] && set -x

source "$(dirname $(readlink -f "${BASH_SOURCE[0]}"))/resources/tools.sh"

CURRENT_BRANCH=${1:-$(git symbolic-ref --short HEAD)}
BASE_BRANCH=${2:-origin/master}
MISSIN_COMMITS=${3:-$(git log $BASE_BRANCH..$CURRENT_BRANCH --pretty=format:%H | tac)}

mux_trace_run git fetch $(echo $BRANCH | cut -d/ -f1)
mux_trace_run git branch -m $CURRENT_BRANCH ${CURRENT_BRANCH}_
mux_trace_run git checkout $BASE_BRANCH -b $CURRENT_BRANCH
mux_trace_run git cherry-pick $MISSIN_COMMITS
mux_trace_run git branch -D ${CURRENT_BRANCH}_
