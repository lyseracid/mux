#!/usr/bin/env python

import jira

EPIK_LINK = 'customfield_10009'


class JiraActons:
    def __init__(self, url: str, credentials: str):
        self._jira = jira.JIRA(server=url, basic_auth=credentials.split(':'))

    def do(self, action, *args):
        '''Does :action(*args)'''
        return getattr(self, action)(*args)

    def fields(self, issueKey):
        '''Show issue fields by :issueKey'''
        issue = self._jira.issue(issueKey)
        return {n: getattr(issue.fields, n) for n in dir(issue.fields) if not n.startswith('_')}

    def children(self, issueKey):
        '''Lists children of :issueKey'''
        issue = self._jira.issue(issueKey)
        result = [link.outwardIssue.key
            for link in issue.fields.issuelinks if link.type.name == 'Parent']
        print("Issue {} has {} children".format(issueKey, len(result)))
        return result

    def set_epic(self, epic, action, *args):
        '''Sets :epic to issues from :action(*args)'''
        for key in self.do(action, *args):
            issue = self._jira.issue(key)
            if not getattr(issue.fields, EPIK_LINK):
                issue.update(**{EPIK_LINK: epic})
                print('- {} - updated'.format(issue.key))

    def diff_epic(self, epics, action, *args):
        '''Prints issues with different then :epics from :action(*args)'''
        for key in self.do(action, *args):
            epic = getattr(self._jira.issue(key).fields, EPIK_LINK, None)
            if not epic or epic not in epics:
                print('- {} - epic: {}'.format(key, epic))



if __name__ == '__main__':
    import argparse
    import os
    from pprint import pprint

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-u', '--url', default=os.getenv('MUX_JIRA_URL'), help='JIRA Server URL')
    parser.add_argument('-c', '--credentials', default=os.getenv('MUX_JIRA_CREDENTIALS'), help='login:password')
    parser.add_argument('action', help='\n'.join([
        '{} - {}'.format(n, f.__doc__) for n, f in JiraActons.__dict__.items() if not n.startswith('_')]))
    parser.add_argument('args', nargs='*', default=[], help='action arguments')

    args = parser.parse_args()
    assert args.url and args.credentials, 'URL and credentials should be provided'

    actions = JiraActons(args.url, args.credentials)
    pprint(actions.do(args.action, *args.args))
