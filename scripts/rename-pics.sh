#!/bin/bash

SUFFIXES="PXL_ IMG_ IMG- VID_ VID- InShot_"

if [[ "$1" == --help ]] || [[ "$1" == -h ]]; then cat <<END
Moves and renames all files to same format grabbed from android phone
Usage:
    [MV=mv] [S='$SUFFIXES'] [T=img_] [PH=1] $0
Options:
    MV  move command
    S   suffixes to replace
    T   target suffix
    PH  also process hidden files and dirs
END
exit 0; fi

set -e
[ "$X" ] && set -x

MV=${MV:-mv}
FILES="find -type f"
SUFFIXES=${S:-$SUFFIXES}

# Build python replace method call chain.
REPLACE=
for S in $SUFFIXES; do
    REPLACE+=".replace('''$S''', '''${T:-img_}''')"
done

ROLLBACK=/tmp/mux-rename-pics.rollback
echo '#!/bin/bash' > $ROLLBACK
chmod +x $ROLLBACK

$FILES | while read -d $'\n' NAME; do
    # Ignore files and dirs with hidden sufixes if specified.
    [ ! -z $PH ] && echo $NAME | grep -v '/\.' > /dev/null && continue

    RENAME=$(python -c "print('''$(basename $NAME)'''$REPLACE)")
    [[ "$NAME" == "$RENAME" ]] && continue

    echo Rename: $NAME
    echo --- to: $RENAME

    $MV "$NAME" "$RENAME"
    echo $MV \"$RENAME\" \"$NAME\" >> $ROLLBACK
done

echo echo Rolled back >> $ROLLBACK
echo Done, rollback: $ROLLBACK
